import cv2

face_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')

def detect_and_blur_faces(img):
     _, img = video_capture.read()

     gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

     faces = face_cascade.detectMultiScale(gray, 1.1, 4)

     for (x, y, w, h) in faces:
          #cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)
          face = img[y:y + h, x:x + w]
          face = cv2.GaussianBlur(face, (23, 23), 30)

          img[y:y + face.shape[0], x:x + face.shape[1]] = face
